/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.datastructures.st1523;
import java.util.Arrays;
import mx.edu.utr.datastructures.*;

/**
 *
 * @author Vidal Zuazo
 */
public class ArrayList implements List {
    
    /**
     * I initialize an array of objects and integer that it will save the array size.
     */
    private Object [] elements;
    private int size;
    
    /**
     * This method establishes an initial capacity of the arrayList.
     * @param inictialCapacity 
     */
    public ArrayList (int inictialCapacity) {
        elements = new Object [inictialCapacity];
    }
    
    /**
     * This method is part of establish an initial capacity, this method say that ArrayList initial capacity is 10.
     */
    public ArrayList (){
        this(10);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(Object element) {
        ensureCapacity (size + 1);
        elements [size++] = element;
        return true;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void add(int index, Object element) {
        rangeCheckAdd(index);
        ensureCapacity(size + 1);
        for (int i = size; i >= index; i--) {
            elements[i + 1] = elements[i];
        }
        elements[index] = element;
        size++;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        for (int i = 0; i < elements.length; i++) {
            elements [i] = null;
            size = 0;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object get(int index) {
        rangeCheck(index);
        return elements [index];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int indexOf(Object element) {
        if (element == null) {
            for (int i = 0; i < size; i++) {
                if (elements[i] == null){
                    return i;
                }
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (element.equals(elements[i])){
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object remove(int index) {
        rangeCheck(index);
        Object old = elements[index];
        int indexM = size - index - 1;
        if (indexM > 0){
            System.arraycopy(elements, index + 1, elements, index, indexM);
        }
        elements[--size] = null;
        return old;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object set(int index, Object element) {
        rangeCheck(index);
        Object old = elements [index];
        elements [index] = element;
        return old;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * It check if the list is enough, if not add more capacity. The "copyOf" allows to save
     * the elements of the set that it had and adds the newcapacity of the array. It will have
     * the ability to duplicate when it will be exceeded.
     * @param minCapacaity 
     */
    private void ensureCapacity(int minCapacaity) {
        int oldCapacity = elements.length;
        if (minCapacaity > oldCapacity) {
            int newCapacity = oldCapacity * 2;
            if (newCapacity < minCapacaity) {
                newCapacity = minCapacaity;
            }
            elements = Arrays.copyOf(elements, newCapacity);
        }
    }
    
    private void rangeCheck (int index) {
        if (index < 0 || index > size -1) {
            throw new IndexOutOfBoundsException();
        }
    }
    
    /**
     * This method it is encharge to be sure if the element is in the list.
     * @param index 
     */
    
    private void rangeCheckAdd (int index) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("Out of bound.");
        }
    }
    
    /**
     * This method prints all the elements in the list.
     */
    public void Print () {
        for (int i = 0; i < elements.length; i++) {
            System.out.println(elements[i]);
        }
    }
    
}
